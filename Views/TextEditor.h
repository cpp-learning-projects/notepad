//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QTextEdit>

class TextEditor : public QTextEdit
{
public:
    TextEditor(QWidget* parent);
};



#endif //TEXTEDITOR_H
