//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "MainWindow.h"
#include "../Views/TextEditor.h"
#include "../Services/FileManager.h"
#include "../Services/MenuManager.h"


MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent)
{
    fileManager = new FileManager();
    textEditor = new TextEditor(this);
    menuManager = new MenuManager(this, textEditor, fileManager);
    menuManager->setupMenus();
    setCentralWidget(textEditor);
}

