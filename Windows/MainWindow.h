//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class FileManager;
class TextEditor;
class MenuManager;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget* parent = nullptr);

private:
    FileManager* fileManager;
    TextEditor* textEditor;
    MenuManager* menuManager;
};
#endif //MAINWINDOW_H
