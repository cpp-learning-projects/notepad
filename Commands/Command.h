//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef COMMAND_H
#define COMMAND_H

class Command
{
public:
    virtual ~Command() = default;
    virtual void execute() = 0;
};

#endif //COMMAND_H
