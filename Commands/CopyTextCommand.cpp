//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "CopyTextCommand.h"
#include "../Views/TextEditor.h"

void CopyTextCommand::execute()
{
    textEditor->copy();
}