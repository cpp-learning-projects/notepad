//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "PasteTextCommand.h"

#include "../Views/TextEditor.h"

void PasteTextCommand::execute()
{
    textEditor->paste();
}