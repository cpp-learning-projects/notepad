//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef CLOSEAPPCOMMAND_H
#define CLOSEAPPCOMMAND_H
#include "Command.h"


class CloseAppCommand : public Command{

public:
    void execute() override;
};



#endif //CLOSEAPPCOMMAND_H
