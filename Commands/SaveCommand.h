//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef SAVECOMMAND_H
#define SAVECOMMAND_H
#include "Command.h"

class QWidget;
class TextEditor;
class FileManager;

class SaveCommand : public Command
{
private:
    QWidget* widget;
    TextEditor* textEditor;
    FileManager* fileManager;

public:
    SaveCommand(TextEditor* textEditor, FileManager* fileManager, QWidget* widget)
        : textEditor(textEditor), fileManager(fileManager), widget(widget) {}
    void execute() override;
};


#endif //SAVECOMMAND_H
