//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "SaveCommand.h"
#include <QFileDialog>
#include "../Services/FileManager.h"
#include "../Views/TextEditor.h"

void SaveCommand::execute()
{
    QString fileName = QFileDialog::getSaveFileName(widget);
    if (!fileName.isEmpty())
    {
        fileManager->saveFile(&fileName, textEditor->toPlainText());
    }
}
