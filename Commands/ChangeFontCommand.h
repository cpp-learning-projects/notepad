//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef CHANGEFONTCOMMAND_H
#define CHANGEFONTCOMMAND_H
#include "Command.h"


class QWidget;
class TextEditor;

class ChangeFontCommand : public Command{

private:
    TextEditor* textEditor;
    QWidget* widget;
public:
    ChangeFontCommand(TextEditor* textEditor, QWidget* widget) : textEditor(textEditor), widget(widget) {}
    void execute() override;
};



#endif //CHANGEFONTCOMMAND_H
