//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef OPENCOMMAND_H
#define OPENCOMMAND_H

#include "Command.h"

class QWidget;
class TextEditor;
class FileManager;

class OpenCommand : public Command
{
private:
    QWidget* widget;
    TextEditor* textEditor;
    FileManager* fileManager;

public:
    OpenCommand(TextEditor* textEditor, FileManager* fileManager, QWidget* widget)
        : textEditor(textEditor), fileManager(fileManager), widget(widget) {}
    void execute() override;
};

#endif //OPENCOMMAND_H
