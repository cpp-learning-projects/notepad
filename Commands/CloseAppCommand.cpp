//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "CloseAppCommand.h"

#include <QCoreApplication>

void CloseAppCommand::execute()
{
    QCoreApplication::quit();
}
