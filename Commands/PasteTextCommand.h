//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef PASTETEXTCOMMAND_H
#define PASTETEXTCOMMAND_H
#include "Command.h"


class TextEditor;

class PasteTextCommand : public Command{
private:
    TextEditor* textEditor;
public:
    PasteTextCommand(TextEditor* textEditor) : textEditor(textEditor) {}
    void execute() override;
};



#endif //PASTETEXTCOMMAND_H
