//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef COPYTEXTCOMMAND_H
#define COPYTEXTCOMMAND_H
#include "Command.h"


class TextEditor;

class CopyTextCommand : public Command{

private:
    TextEditor* textEditor;
public:
    CopyTextCommand(TextEditor* textEditor) : textEditor(textEditor) {}
    void execute() override;
};



#endif //COPYTEXTCOMMAND_H
