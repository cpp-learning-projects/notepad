//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "OpenCommand.h"
#include <QFileDialog>
#include "../Services/FileManager.h"

void OpenCommand::execute()
{
    QString fileName = QFileDialog::getOpenFileName(widget);
    if (!fileName.isEmpty())
    {
        fileManager->openFile(&fileName, textEditor);
    }
}
