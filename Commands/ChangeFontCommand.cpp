//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "ChangeFontCommand.h"
#include <QFontDialog>
#include "../Views/TextEditor.h"

void ChangeFontCommand::execute()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, textEditor->font(), widget);
    if (ok)
    {
        textEditor->setFont(font);
    }
}
