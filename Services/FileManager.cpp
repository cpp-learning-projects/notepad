//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "FileManager.h"

#include <qfile.h>
#include "../Views/TextEditor.h"

void FileManager::openFile(QString* path, TextEditor* textEditor)
{
    QFile file(*path);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        textEditor->setText(in.readAll());
        file.close();
    }
}

void FileManager::saveFile(QString* path, QString content)
{
    QFile file(*path);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&file);
        out << content;
        file.close();
    }
}
