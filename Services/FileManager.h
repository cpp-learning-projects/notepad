//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef FILEMANAGER_H
#define FILEMANAGER_H

class QString;
class TextEditor;

class FileManager
{
public:
    void openFile(QString* path, TextEditor* textEditor);
    void saveFile(QString* path, QString content);
};



#endif //FILEMANAGER_H
