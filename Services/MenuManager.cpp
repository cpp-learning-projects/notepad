//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "MenuManager.h"
#include <QMenuBar>

#include "../Commands/ChangeFontCommand.h"
#include "../Commands/CloseAppCommand.h"
#include "../Commands/CopyTextCommand.h"
#include "../Commands/OpenCommand.h"
#include "../Commands/PasteTextCommand.h"
#include "../Commands/SaveCommand.h"
#include "../Windows/MainWindow.h"

MenuManager::MenuManager(MainWindow* mainWindow, TextEditor* textEditor, FileManager* fileManager)
    :mainWindow(mainWindow), textEditor(textEditor), fileManager(fileManager)
{
    menuBar = new QMenuBar(mainWindow);
}

void MenuManager::setupMenus()
{
    createFileMenu();
    createEditMenu();
    createTextMenu();
    mainWindow->setMenuBar(menuBar);
}

void MenuManager::createFileMenu()
{
    QMenu* fileMenu = menuBar->addMenu(QObject::tr("File"));

    OpenCommand* openCommand = new OpenCommand(textEditor, fileManager, mainWindow);
    QAction* openAction = fileMenu->addAction(QObject::tr("&Open"));
    QObject::connect(openAction, &QAction::triggered, [openCommand]()
    {
        openCommand->execute();
    });

    SaveCommand* saveCommand = new SaveCommand(textEditor, fileManager, mainWindow);
    QAction* saveAction = fileMenu->addAction(QObject::tr("&Save"));
    QObject::connect(saveAction, &QAction::triggered, [saveCommand]()
    {
        saveCommand->execute();
    });
    
    fileMenu->addSeparator();

    CloseAppCommand* closeAppCommand = new CloseAppCommand();
    QAction* closeAction = fileMenu->addAction(QObject::tr("&Close"));
    QObject::connect(closeAction, &QAction::triggered, [closeAppCommand]()
    {
        closeAppCommand->execute();
    });
}

void MenuManager::createEditMenu()
{
    QMenu* editMenu = menuBar->addMenu(QObject::tr("Edit"));

    CopyTextCommand* copyCommand = new CopyTextCommand(textEditor);
    QAction* copyAction = editMenu->addAction(QObject::tr("&Copy"));
    QObject::connect(copyAction, &QAction::triggered, [copyCommand]()
    {
        copyCommand->execute();
    });

    PasteTextCommand* pasteCommand = new PasteTextCommand(textEditor);
    QAction* pasteAction = editMenu->addAction(QObject::tr("&Paste"));
    QObject::connect(pasteAction, &QAction::triggered, [pasteCommand]()
    {
        pasteCommand->execute();
    });
}

void MenuManager::createTextMenu()
{
    QMenu* textMenu = menuBar->addMenu(QObject::tr("Text"));

    ChangeFontCommand* changeFontCommand = new ChangeFontCommand(textEditor, mainWindow);
    QAction* changeFontAction = textMenu->addAction(QObject::tr("&Change font"));
    QObject::connect(changeFontAction, &QAction::triggered, [changeFontCommand]()
    {
        changeFontCommand->execute();
    });
}
