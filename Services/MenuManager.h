//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef MENUMANAGER_H
#define MENUMANAGER_H

class QMenuBar;
class FileManager;
class TextEditor;
class MainWindow;

class MenuManager {
public:
    MenuManager(MainWindow* mainWindow, TextEditor* textEditor, FileManager* fileManager);
    void setupMenus();

private:
    MainWindow* mainWindow;
    TextEditor* textEditor;
    FileManager* fileManager;
    QMenuBar* menuBar;

    void createFileMenu();
    void createEditMenu();
    void createTextMenu();
};
#endif //MENUMANAGER_H
