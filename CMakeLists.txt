cmake_minimum_required(VERSION 3.28)
project(Notepad)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_AUTOMOC ON)
find_package(Qt6Widgets REQUIRED)

add_executable(Notepad main.cpp
        Windows/MainWindow.cpp
        Windows/MainWindow.h
        Views/TextEditor.cpp
        Views/TextEditor.h
        Services/FileManager.cpp
        Services/FileManager.h
        Services/MenuManager.cpp
        Services/MenuManager.h
        Commands/Command.h
        Commands/OpenCommand.cpp
        Commands/OpenCommand.h
        Commands/SaveCommand.cpp
        Commands/SaveCommand.h
        Commands/CloseAppCommand.cpp
        Commands/CloseAppCommand.h
        Commands/ChangeFontCommand.cpp
        Commands/ChangeFontCommand.h
        Commands/CopyTextCommand.cpp
        Commands/CopyTextCommand.h
        Commands/PasteTextCommand.cpp
        Commands/PasteTextCommand.h
)

target_link_libraries(Notepad Qt6::Widgets)